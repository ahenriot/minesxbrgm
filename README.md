# minesXbrgm
Projet de collaboration entre le BRGM et les mines, dans le cadre des projets étudiants.

Le défi proposé pour 2023 est spécifié dans le notebook 'Defi_BRGM_temperature_eau_souterraine'.

# Authors
Abel Henriot, BRGM, a.henriot@brgm.fr
Agnes Rivière,Mines Paris, Centre Géosciences, agnes.riviere@minesparis.psl.eu